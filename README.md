#Game'zone
## Anthony // Chris // Jason // Julien

Projet du site du parc d'attraction Game'zone

Répartition des taches

Veille concurrentiel individuelle pour tous

Jason => Prototype maquette page d'accueil, maquette page d'accueil finale, page "Le parc", page "Dans la presse", page "BIlleterie" et "calendrier"

Anthony => Prototype maquette page d'accueil, page "Attractions", page "Conditions légales", page "Conditions de ventes", cahier des charges technique.

Chris => maquette page d'accueil, maquette "Le parc", maquette "Attraction", Prototype page "My games", page "Information", page "Venir au parc", page "on recrute"

Julien => Nomenclature de chaque page (header/navbar/footer), système d'inscription/connexion, page d'accueil, page "Plan", page d'accueil + nav + footer en ANGLAIS, page d'accueil + nav + footer en ESPAGNOL , profil,


lien du trello => https://trello.com/b/I5pHLsRL/gamezone

lien du marvelapp => https://marvelapp.com/project/3288305/
