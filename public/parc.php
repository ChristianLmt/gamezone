<?php session_start() ; ?>
<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="style/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Satisfy" rel="stylesheet"> 
  <title>Game'zone</title>
</head>

<body>
  <header>
  <?php include 'nav.php' ; ?>
  </header>
    <div class="page">
      <h1>Game'Zone</h1>
      <div class="container">
        <div class="row">
          <div class="col">
            <h2> Notre Parc !</h2>
            <br>
            <p>Un parc sur le thème du jeu vidéo, Un projet unique qui vous permettra de découvrir le jeu vidéo sous un jour nouveau et de vivre des expériences uniques au monde. Devenez acteur de votre passion et partagez cette aventure en étant connecté avec le monde entier. En famille ou entre amis, ce parc vous offrira une grande variété d’attractions en cotoyant vos héros favoris.</p>
          </div>
          <div class="col">
            <h2>Amusez-vous !</h2>
            <br>
            <p>Évadez vous grâce à la réalité virtuelle, parcourez le musée du jeu vidéo et découvrez l’histoire de votre passion. Pilotez une formule 1 grâce à des simulateurs, rencontrez les héros de vos jeux vidéo en hologrammes, faites le plein de sensations fortes dans des attractions à couper le souffle, les toutes dernières technologies numériques au service de vos loisirs.</p>
          </div>
        </div>
      </div>
      <img src="img/photo_parc3.jpg" width="80%" alt="photo_du_parc">
      <div class="container">
        <div class="row-6">
          <h2> Le Plan du Parc </h2>
          <br>
          <img src="img/plan_caroussel.jpg" width="80%" height="50%" alt="plan_du_parc">
        </div>
      </div>
      <div class="container">
        <div class="row-fluid">
          <h2> Acheter vos billets </h2>
          <center>
          <span>Venez préparer votre visiter et acheter vos billets</span>
          <a class="nav-link" href="price.php">ICI</a>
        </center>
        </div>
      </div>
    </div>
    <br>

    <footer>
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <ul>
              <li><a href="parc.php>">A Propos du parc</a></li>
              <li><a href="presse.php">Dans la presse</a></li>
              <li><a href="recrutement.php">On recrute</a></li>
            </ul>
          </div>
          <div class="col">
            <ul>
              <li><a href="mailto:contact@gamezone.com">Nous contacter</a></li>
              <li><a href="conditions_ventes.php">Conditions de vente</a></li>
              <li><a href="conditions_legales.php">Conditions légales</a></li>
            </ul>
          </div>
          <div class="col">
            <ul>
              <li>Du lundi au jeudi : 9h-19h</li>
              <li>Du vendredi au samedi : 9h-20h</li>
              <li>Le dimanche : 9h – 18h</li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <img src="img/logo_facebook.png" alt="logo_facebook" width="50%">
          </div>
          <div class="col">
            <img src="img/logo_twitter.png" alt="logo_twitter" width="50%">
          </div>
          <div class="col">
            <img src="img/logo_insta.png" alt="logo_insta" width="50%">
          </div>
          <div class="col">
            <img src="img/logo_youtube.png" alt="logo_youtube" width="50%">
          </div>
          <div class="col-7">
            <form class="input-group">
              <input type="text" class="form-control form-control-sm" placeholder="Votre mail" aria-label="Your email" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-sm btn-outline-white" type="button">Inscrivez-vous !</button>
              </div>
            </form>
          </div>


        <div class="row">
          <div class="col">
          <p>©2018 - Tout droits réservés</p>
          </div>
        </div>
    </footer>

      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
    </html>
