<?php session_start() ; ?>
<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="style/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Satisfy" rel="stylesheet"> 
  <title>Game'zone</title>
</head>

<body>
  <header>
  <?php include 'nav.php' ; ?>
  </header>

<div class="page">

<p class="conditions_vente">
  <h2>Conditions de vente</h2> <br>

Vous pouvez acheter vos billets directement sur le site. <br>

Composez votre panier avec les entrées désirées, <br>

Une fois votre achat finalisé vous recevrez un courrier de confirmation. <br>

Via la page mon compte vous povez télécharger et imprimer vos billets. <br> <br>



<strong>Remboursement</strong> <br>
Les abonnements et offres internet ne sont pas remboursables et doivent être utilisés dans la saison de l'achat. <br> <br>

<strong>Objet</strong> <br>
Les présentes conditions de vente visent à définir les relations contractuelles entre Gamezone et le Client et les conditions applicables à tout achat effectué via le site internet Gamezone.
L’acquisition d’un produit par le biais du site internet Gamezone implique une acceptation sans réserve par le Client des présentes conditions générales de vente. <br><br>

<strong>Caractéristiques des produits proposés</strong> <br>
Les produits proposés sont ceux figurant dans le catalogue publié dans le site internet de Gamezone. Ces produits sont proposés dans la limite des stocks disponibles.
L’état de stock de chaque produit est renseigné par des pictogrammes de couleur : <br>

En vert : produit disponible. <br>

En rouge : produit indisponible. <br>

Chaque produit fait l’objet d’un descriptif détaillé selon les informations du fournisseur. Les informations de taille et de poids des produits sont retransmises à titre indicatif car elles sont converties et arrondies (unités de mesure inch et once vers unités de mesure cm et gramme) pour une meilleure compréhension.

Les photographies du catalogue se veulent les plus fidèles possibles mais elles ne peuvent assurer une similitude parfaite avec le produit proposé, notamment en ce qui concerne les couleurs et les nuances de teintes. <br><br>

<strong>Tarifs</strong> <br>
Les prix figurant dans le catalogue sont des prix TTC en euros tenant compte de la TVA applicable au jour de la commande. Tout changement du taux pourra être répercuté sur le prix des produits.

Parc de la Vallée se réserve le droit de modifier ses prix à tout moment. Toutefois, il est entendu que le prix figurant au catalogue le jour de la commande sera le seul applicable au Client. <br><br>

<strong>Commandes</strong> <br>
Le Client qui souhaite acheter un produit sur le site internet Gamezone doit obligatoirement :

Créer son espace personnel sur le site internet Gamezone en suivant la procédure d’inscription en ligne. Le Client possédant déjà son espace personnel sur le site internet Gamezone devant simplement s’y connecter.

Remplir son panier en ligne en choisissant les produits et en les ajoutant au panier.

Vérifier le contenu de son panier et effectuer les éventuelles modifications avant de finaliser la commande. La validation de la commande formalise la vente entre Gamezone et le Client.

Effectuer le paiement selon le mode de paiement choisi et les conditions prévues.

La confirmation de la commande entraîne acceptation des présentes conditions de vente, la reconnaissance d’en avoir parfaite connaissance et la renonciation à se prévaloir de ses propres conditions d’achat ou d’autres conditions.

L’ensemble des données fournies et la confirmation enregistrée vaudront preuve de la transaction. La confirmation vaudra signature et acceptation des opérations effectuées.

Gamezone communiquera par courrier électronique confirmation de la commande enregistrée. <br><br>

<strong>Paiement</strong> <br>
Pour régler le montant de sa commande, le Client peut choisir sur le site internet Gamezone parmi plusieurs moyens de paiement :

Par carte bancaire : Carte Bleue, Visa, Mastercard. Cette solution de paiement sécurisé des banques CIC utilise le protocole de sécurité renforcée 3D SECURE

La commande ne sera traitée qu’à compter du jour de l’encaissement réel du chèque.

Gamezone se réserve la possibilité d’annuler toute commande dont le règlement ne lui aurait pas été adressé dans les sept jours suivant la commande.

Par virement bancaire à Gamezone dont les coordonnées bancaires figurent sur la page de règlement dans le tunnel de commande du site Gamezone .

La commande ne sera traitée qu’à compter du jour de réception du virement.

Gamezone se réserve la possibilité d’annuler toute commande dont le virement ne lui serait pas parvenu dans les sept jours suivant la commande.

Gamezone se réserve le droit de ne pas honorer une commande effectuée par un Client qui n’aurait pas réglé totalement ou en partie une précédente commande où bien qui ferait déjà l’objet d’un litige de paiement en cours. <br><br>

<strong>Rétractation</strong> <br>
Conformément à l’article L. 121-20 et suivants du Code de la Consommation, le Client bénéficie d’un délai de rétractation de quatorze jours à compter de la livraison de la commande pour faire retour du produit à Gamezone pour échange ou remboursement sans pénalité. Les frais de retour sont à la charge du Client.

Le produit retourné par le Client doit être neuf, non utilisé, dans son état initial et dans son emballage d’origine non ouvert. <br><br>

<strong>Garantie</strong> <br>
L’ensemble des produits vendus par Gamezone bénéficie de la garantie légale prévue par les articles 1641 et suivants du Code Civil, de la garantie légale de conformité en vertu des articles L211-4 du Code de la Consommation et des garanties fournies par les constructeurs des produits.

Le Client qui souhaite faire prévaloir ses droits devra informer Gamezone de l’existence de vices par courrier recommandé avec accusé de réception dans les délais prévus par l’article 1648 du Code Civil et les articles L211-12 du code de la Consommation, sans quoi le Client serait déchu de toute action s’y rapportant. <br><br>



<strong>Responsabilité</strong> <br>


Dans le processus de vente en ligne, Gamezone n’est tenu que par une obligation de moyens. Sa responsabilité ne pourra être engagée pour un dommage résultant de l’utilisation du réseau internet tel que perte de données, intrusion, virus, rupture du service ou autres problèmes involontaires. <br><br>

<strong>Force majeure</strong> <br>
Gamezone ne saurait être tenu responsable pour tout manquement à ses obligations contractuelles résultant d’un cas de force majeure ou fortuite tels que grèves, incendies, inondations, catastrophes naturelles, cette liste n’étant pas exhaustive. <br><br>

<strong>Propriété intellectuelle</strong> <br>
L’ensemble du contenu et des éléments du site internet Gamezone reste la propriété intellectuelle et exclusive de Gamezone .

Toute reproduction, exploitation, rediffusion ou utilisation à quel titre que ce soit, même partiellement, des éléments du site internet qu’ils soient logiciels, visuels ou sonores est interdite sauf autorisation écrite de Gamezone .

De même, tout lien simple ou lien hypertexte est strictement interdit sans un accord écrit de Gamezone. <br><br>

<strong>Données à caractère personnel</strong> <br>
Conformément à la loi relative à l’informatique, aux fichiers et aux libertés du 6 janvier 1978, les informations à caractère nominatif relatives au Client pourront faire l’objet d’un traitement automatisé.

Gamezone ne transmet aucune donnée ni information à des tiers.

Le Client dispose d’un droit d’accès et de rectification des données le concernant conformément à la loi du 6 janvier 1978.

Le traitement automatisé d’informations, y compris la gestion des adresses e-mail des Clients du site internet a fait l’objet d’une déclaration à la CNIL enregistrée sous le numéro (autorisation en cours). <br><br>

<strong>Archivage & Preuve</strong> <br>
Gamezone procédera à l’archivage des commandes sur un support fiable et durable constituant une copie fidèle conformément aux dispositions de l’article 1348 du Code Civil.

Les registres informatisés de Gamezone seront considérés par les parties comme preuve des commandes et transactions intervenues entre les parties. <br><br>

<strong>Intégralité</strong> <br>
Dans le cas où l’une des stipulations des présentes conditions générales de vente serait considérée comme nulle et non avenue du fait d’un changement de réglementation, de législation ou d’une décision définitive de justice, les autres stipulations demeureraient valides et conserveraient toute leur portée. <br><br>

<strong>Règlement des litiges</strong> <br>
Les présentes conditions générales de vente en ligne de produits sont soumises à la loi française.

En cas de litige, nonobstant pluralité de défendeurs ou appel en garantie, la juridiction compétente sera celle définie par les conditions de droit commun.</p>

</div>

<footer>
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <ul>
          <li><a href="parc.php>">A Propos du parc</a></li>
          <li><a href="presse.php">Dans la presse</a></li>
          <li><a href="recrutement.php">On recrute</a></li>
        </ul>
      </div>
      <div class="col">
        <ul>
          <li><a href="mailto:contact@gamezone.com">Nous contacter</a></li>
          <li><a href="conditions_ventes.php">Conditions de vente</a></li>
          <li><a href="conditions_legales.php">Conditions légales</a></li>
        </ul>
      </div>
      <div class="col">
        <ul>
          <li>Du lundi au jeudi : 9h-19h</li>
          <li>Du vendredi au samedi : 9h-20h</li>
          <li>Le dimanche : 9h – 18h</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <img src="img/logo_facebook.png" alt="logo_facebook" width="50%">
      </div>
      <div class="col">
        <img src="img/logo_twitter.png" alt="logo_twitter" width="50%">
      </div>
      <div class="col">
        <img src="img/logo_insta.png" alt="logo_insta" width="50%">
      </div>
      <div class="col">
        <img src="img/logo_youtube.png" alt="logo_youtube" width="50%">
      </div>
      <div class="col-7">
        <form class="input-group">
          <input type="text" class="form-control form-control-sm" placeholder="Votre mail" aria-label="Your email" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-sm btn-outline-white" type="button">Inscrivez-vous !</button>
          </div>
        </form>
      </div>


    <div class="row">
      <div class="col">
      <p>©2018 - Tout droits réservés</p>
      </div>
    </div>
</footer>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
