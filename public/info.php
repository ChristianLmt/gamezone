<?php session_start() ; ?>
<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="style/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Satisfy" rel="stylesheet"> 
  <title>Game'zone</title>
</head>

<body>
  <header>
  <?php include 'nav.php' ; ?>
  </header>


  <h4>Les infos pratiques </h4>

  <div class="container info">
    <div class="row">

      <div class="col element">
        <h3>Coordonées </h3>
        <br>
        <p>GameZone Geek-Center</p>
        <br>
        <p>607 rue Pascal Dugivre <br> France</p>
        <br>
        <p>tel : ++02563 4554 2023 640</p>
        <p>Mail : contact@gamezone.com</p>
        <br>
        <p> <a href="#">Contactez-nous</a> </p>
      </div>

      <div class="col element">
        <h3>Horaire d'ouverture</h3>
        <br>
        <p>Nos heures d'ouverture varient. Consultez le calendrier pour les heures d'ouverture actuelles.</p>
        <br><br>
        <p>Du lundi au jeudi : 9h-19h <br> Du vendredi au samedi : 9h-20h <br> Le dimanche : 9h – 18h</p>
        <br>
        <br>
        <p> <a href="#">Voir le calendrier</a> </p>
      </div>

      <div class="col element">
        <h3>Itinéraire</h3>
        <br>
        <p>Le parc GameZone Geek-Center se trouve en France, entre mon nez et ma bouche</p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d322403.79431527667!2d-1.2618705671875!3d50.8537647!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48745d113df17151%3A0xef32855540bc7d5c!2sGame+Zone!5e0!3m2!1sen!2sfr!4v1535534301000"
          width="290" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
        <p> <a href="#">Vous y rendre </a> </p>
      </div>
    </div>

    <!-- /////////////////////////////////// -->
    <div class="row">

      <div class="col element">
        <h3>Transport</h3>
        <br>
        <p>Consulter l'horaire de transport actuel de <br>notre partenaire BlaBla-car</p>
        <br>
        <br>
        <br><br>
        <br>
        <p> <a href="#"> Venez en transport en commun </a> </p>
      </div>

      <div class="col element">
        <h3>Presse</h3>
        <br>
        <p>Un parc d'attraction dédié au jeu vidéo en projet sur le sol français</p>
        <br><br><br><br><br>
        <p> <a href="http://www.jeuxvideo.com/news/489403/un-parc-d-attraction-dedie-au-jeu-video-en-projet-sur-le-sol-francais.htm">voir les derniere actualité sur GameZone</a> </p>

      </div>

      <div class="col element">
        <h3>Fumer</h3>
        <br>
        <p>Il est interdit de fumer à l'intérieur et dans les files d'attente des attractions (il en vaut de même pour les cigarettes électroniques). Il y a plusieurs zones fumeurs dans le parc où il est permis de fumer</p>

      </div>
    </div>
  </div>



  <footer>
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <ul>
            <li><a href="parc.php>">A Propos du parc</a></li>
            <li><a href="presse.php">Dans la presse</a></li>
            <li><a href="recrutement.php">On recrute</a></li>
          </ul>
        </div>
        <div class="col">
          <ul>
            <li><a href="mailto:contact@gamezone.com">Nous contacter</a></li>
            <li><a href="conditions_ventes.php">Conditions de vente</a></li>
            <li><a href="conditions_legales.php">Conditions légales</a></li>
          </ul>
        </div>
        <div class="col">
          <ul>
            <li>Du lundi au jeudi : 9h-19h</li>
            <li>Du vendredi au samedi : 9h-20h</li>
            <li>Le dimanche : 9h – 18h</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <img src="img/logo_facebook.png" alt="logo_facebook" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_twitter.png" alt="logo_twitter" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_insta.png" alt="logo_insta" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_youtube.png" alt="logo_youtube" width="50%">
        </div>
        <div class="col-7">
          <form class="input-group">
            <input type="text" class="form-control form-control-sm" placeholder="Votre mail" aria-label="Your email" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="btn btn-sm btn-outline-white" type="button">Inscrivez-vous !</button>
            </div>
          </form>
        </div>


      <div class="row">
        <div class="col">
        <p>©2018 - Tout droits réservés</p>
        </div>
      </div>
  </footer>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
