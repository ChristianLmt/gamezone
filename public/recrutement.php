<?php session_start() ; ?>
<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="style/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Satisfy" rel="stylesheet"> 
  <title>Game'zone</title>
</head>

<body>
  <header>
  <?php include 'nav.php' ; ?>
  </header>



  <h4>On recrute !!! </h4>

  <div class="container recrutement">
    <div class="row">
      <div class="col">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">Journaliste High-Tech/Jeux Video</button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Journaliste High-Tech/Jeux Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">

                <p>Durée : 2 ou 6 mois <br> Quand ?Dès Septembre-Octobre 2018 <br>Vous serez dans une équipe dans 3 journalistes à temps plein et 1 autre stagiaire. <br>Missions :</p>
                <ul>
                  <li>Au sein de l’équipe éditoriale du site, vous participerez activement à l’activité éditoriale. </li>
                  <li> À l’écrit : vous rédigerez des articles liés à l’actualité de l'innovation et des objets connectés
                  </li>
                  <li>Assurer une veille éditoriale sur diverses sources d’information et les réseaux sociaux afin de tenir l’équipe au courant des dernières actualités
                  </li>
                  <li>Identifier les contenus, sujets et angles les plus frais et pertinents sur diverses thématiques,</li>
                  <li> Rédiger des articles et les mettre en page (dossiers de fonds, articles, interviews, brèves, focus, tests, voyages des presse...)</li>
                  <li> Couvrir des événements / lancements de produits Réaliser des interviews vidéos / tests produits & jeux </li>
                </ul>
                <p>Votre profil : <br>Sympathique et souriant(e), vous êtes : passionné(e) par Internet (blogging, communautés, réseaux sociaux), de nouvelles technologies, de startups, d’information, de multimédia : en bref, un peu geek, mais pas trop !
                  rigoureux et consciencieux : vous êtes une personne de confiance avec une grande envie d’apprendre disponible, proactif, force de proposition et autonome Fort(e) de vos compétences (idéalement transversales) <br>Vous êtes issu(e) d’une
                  formation en journalisme / communication Vous utilisez régulièrement les logiciels et outils web tels que WordPress, les services Google et les réseaux sociaux. Vous avez de bonnes connaissances en rédaction. Vous aimez travailler en
                  équipe (et surtout en mode startup), détenez un grand sens du contact et gardez toujours le sourire ! Vous avez une bonne capacité rédactionnelle (orthographe et grammaire irréprochables) en français et vous aimez l'innovation, l'high
                  tech et/ou les jeux vidéos.<br> Coordonnées du recruteur : recrutement(arobase)publithings.com (CV + Mail de motivation) <br> Type d'emploi : Stage <br>Langue: français (Requise)</p>
                </p>


              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary btn-sup" data-toggle="modal" data-target="#support_ModalLong">Support Client</button>

        <!-- Modal -->
        <div class="modal fade" id="support_ModalLong" tabindex="-1" role="dialog" aria-labelledby="support_ModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="support_ModalLongTitle">Support Client H/F</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">

                <p>Présentation de l’entreprise : <br> Beemoov figure parmi les leaders mondiaux de jeux cross-plateformes sur navigateur web et mobile.
                  <br> Spécialisé dans l’otome game et fort de ses plus de 80 millions de comptes actifs, notre catalogue regroupe des jeux à succès en constante évolution tels que Ma Bimbo, Amour Sucré ou encore Eldarya. Tous ces jeux sont disponibles
                  en 9 langues à travers le monde.
                  <br> Récapitulatif du poste :<br>
                  <br> Support Client :
                  <br>
                  <br> Répondre à l’ensemble des questions des clients concernant l’ensemble de nos produits Répondre en priorité aux Litiges de paiements. Gérer les agents externes pour les réponses les plus simples. Gérer le contenu d’aide client à
                  travers la rédaction et mise à jour d’une FAQ et le forum. Reporting :
                  <br> Faire un reporting des avis de nos clients sur nos produits au près de nos équipes. Community Management
                  <br> Sur la direction du Responsable de communication, créer, animer & modérer les réseaux sociaux & forums. Recruter & gérer les modérateurs sur les versions françaises. Profil recherché :
                  <br> Énergique, ouvert, communiquant et possédant une bonne capacité d’adaptation.
                  <br> Une première expérience dans le SAV d’un groupe spécialisé dans l’informatique ou dans le secteur des jeux vidéo serait un plus.
                  <br> Qualités :
                  <ul>
                    <li> Patient</li>
                    <li> Rigoureux</li>
                    <li> Méthodique</li>
                    <li>Bon relationnel</li>
                    <li> Passionné par l’univers des nouvelles technologies et des jeux vidéo</li>
                  </ul>
                  Disponibilité : A pourvoir dès que possible. Type d'emploi : CDD </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#testeurModalLong">Testeur Jeux Vidéo - CDD H/F</button>

        <!-- Modal -->
        <div class="modal fade" id="testeurModalLong" tabindex="-1" role="dialog" aria-labelledby="testeurModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="testeurModalLongTitle">Support Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <p>Le/la testeur(se) a un rôle très important car c’est l’un des derniers maillons de la chaîne de production du jeu vidéo. En tant que testeur(se), vous repérez les points faibles et les défauts du jeu. Pour cela, vous testez nos applications
                  et jeux, assurez le suivi des bugs, et rédigez des comptes rendus.<br> Afin de renforcer notre équipe, nous recherchons notre futur(e) testeur(se). Pour ce faire, en étroite collaboration avec les équipes de production de nos jeux vidéo,
                  <br><br> vous intervenez sur les missions suivantes :
                  <ul>


                    <li>Tester des applications de nos jeux vidéo, suivant des procédures préétablies</li>
                    <li> Assurer le suivi des bugs que vous aurez vous-même découverts mais également ceux rapportés par les joueurs ou les outils internes</li>
                    <li> Rédiger des comptes rendus sur la base de remarques, suggestions ou préconisations dans le cadre d'un Processus Qualité</li>
                    <li>Vérifier la bonne prise en compte et correction des bugs découverts avant de valider les différentes versions de nos produits</li>
                    <li> Observer les comportements des joueurs et analyser les résultats obtenus en rédigeant des rapports</li>
                  </ul>
                  <br><br>Profil souhaité <br> Vous justifiez au moins d’une première expérience dans le test ou dans un domaine proche de la production de jeux vidéo
                  <br> Votre culture vidéoludique ainsi que votre connaissance de nos jeux (principalement Wakfu, Dofus et Dofus Touch) sont des atouts qui seront appréciés
                  <br> Parce que le savoir-être est très important pour nous, vous disposez d’un bon relationnel, êtes méthodique et savez prendre du recul sur votre travail et de manière plus générale sur les projets
                  <br> Contrat : CDD 6 mois renouvelable</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#assistantModalLong">Assistant Chef Produit Jeux Vidéo</button>

        <!-- Modal -->
        <div class="modal fade" id="assistantModalLong" tabindex="-1" role="dialog" aria-labelledby="assistantModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="ModalLongTitle">Assistant Chef Produit Jeux Vidéo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <p>Sous la responsabilité d'un Chef Produit, au sein de la division marketing jeux vidéo, vous prenez part à toutes les étapes du marketing de nos jeux : analyse du marché, conception du plan marketing, production des éléments de communication,
                  et soutien à la commercialisation.<br><br> Vos missions sont les suivantes :

                  <ul>
                    <li>Assister le Chef Produit dans toutes ses tâches et missions</li>
                    <li>Assurer la bonne production et le relais des éléments du plan marketing</li>
                    <li>Faire valider les éléments de communication par les différents ayants droit (licencieurs, consoliers...)</li>
                    <li>Soutenir la force de vente avec les outils d'aide à la vente adaptés</li>
                    <li>Aider à l'organisation des salons professionnels</li>
                    <li>Assurer la veille, les analyses de marché ainsi que les rapports internes</li>
                  </ul><br>Profil du candidat <br> Vous êtes étudiant en marketing en 2ème ou 3ème année d'une grande école de commerce.<br> Structuré, rigoureux, inventif, vous avez une bonne culture du jeu vidéo et êtes sensible aux contraintes de développement
                  des jeux vidéo, de leur édition et de leur distribution. Vous êtes force de proposition et savez communiquer tant à l'oral qu'à l'écrit. Une première expérience dans le domaine du jeu vidéo est un plus.<br><br> Anglais courant <br><br>                  Informations supplémentaires <br> Rémunération : 850 euros mensuel brut <br> Poste à pourvoir : dès que possible, pour une durée de 6 mois.<br></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>


  <div class="container carousel">
    <div class="row">
      <div class="col">
        <p class="accroche"> <i>"Nous sommes une entreprise à taille humaine et nous privilégions la qualité de nos services. Bref, nous sommes des passionnés de jeux video."</i></p>
      </div>

    </div>
    <div class="row">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="img/team_1.jpg" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/team_2.jpg" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/team_3.jpg" alt="Third slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/team_4.jpg" alt="Fourth slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>


  <div class="container">
    <div class="row team_block">
      <div class="xs_1_1">
        <h2>Pourquoi rejoindre notre équipe ?</h2>
      </div>
    </div>

    <div class="row">

      <div class="col">
        <div class="xs_1_1 sm_1_2 md_1_4">
          <div class="box">
            <p class="text-center" style="color: #6caed0;"> <img src="img/malette.png" alt=""> </p>
            <p class="text-center">Une bonne ambiance de travail</p>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="xs_1_1 sm_1_2 md_1_4">
          <div class="box">
            <p class="text-center" style="color: #bad344;"> <img src="img/montre.png" alt=""> </p>
            <p class="text-center">Un état d'esprit Start-up</p>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="xs_1_1 sm_1_2 md_1_4">
          <div class="box">
            <p class="text-center" style="color: #6caed0;"> <img src="img/coffee.png" alt=""> </p>
            <p class="text-center">Du café et des boissons à volonté</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div class="xs_1_1 sm_1_2 md_1_4">
          <div class="box">
            <p class="text-center" style="color: #cc5968;"> <img src="img/idea.png" alt=""> </p>
            <p class="text-center">Des cerveaux créatifs</p>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="xs_1_1 sm_1_2 md_1_4">
          <div class="box">
            <p class="text-center" style="color: #6caed0;"> <img src="img/star.png" alt=""> </p>
            <p class="text-center">Un nom reconnu dans le secteur</p>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="xs_1_1 sm_1_2 md_1_4">
          <div class="box">
            <p class="text-center" style="color: #bad344;"> <img src="img/world.png" alt=""> </p>
            <p class="text-center">Un environnement international</p>
          </div>
        </div>
      </div>
      <hr>
    </div>
    <div class="team_block">
      <div>
        <h2 class="text-center">Envie de tenter l'aventure ?</h2>
      </div>
      <div class="text-center"><a class="btn btn-success btn-lg" href="">Nous rejoindre</a></div>
    </div>
  </div>





  <footer>
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <ul>
            <li><a href="parc.php>">A Propos du parc</a></li>
            <li><a href="presse.php">Dans la presse</a></li>
            <li><a href="recrutement.php">On recrute</a></li>
          </ul>
        </div>
        <div class="col">
          <ul>
            <li><a href="mailto:contact@gamezone.com">Nous contacter</a></li>
            <li><a href="conditions_ventes.php">Conditions de vente</a></li>
            <li><a href="conditions_legales.php">Conditions légales</a></li>
          </ul>
        </div>
        <div class="col">
          <ul>
            <li>Du lundi au jeudi : 9h-19h</li>
            <li>Du vendredi au samedi : 9h-20h</li>
            <li>Le dimanche : 9h – 18h</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <img src="img/logo_facebook.png" alt="logo_facebook" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_twitter.png" alt="logo_twitter" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_insta.png" alt="logo_insta" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_youtube.png" alt="logo_youtube" width="50%">
        </div>
        <div class="col-7">
          <form class="input-group">
            <input type="text" class="form-control form-control-sm" placeholder="Votre mail" aria-label="Your email" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="btn btn-sm btn-outline-white" type="button">Inscrivez-vous !</button>
            </div>
          </form>
        </div>


      <div class="row">
        <div class="col">
        <p>©2018 - Tout droits réservés</p>
        </div>
      </div>
  </footer>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>

</html>
