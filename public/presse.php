<?php session_start() ; ?>
<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="style/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Satisfy" rel="stylesheet"> 
  <title>Game'zone</title>
</head>

<body>
  <header>
  <?php include 'nav.php' ; ?>
  </header>
  <div class="page">
    <h1>Game'Zone</h1>
    <div class="container">
      <div class="row-6">
        <h2>Dans la presse</h2>
        <div class="col">
          <br>
          <div class="card" class="card mb-3">
            <img src="img/esport.jpg" width="75%" alt="esport">
            <div class="card-body">
              <h2 class="card-title">L'E-sport dans Gamezone</h2>
              <p class="card-text">De nombreuses activités et attractions vous attendent dans le parc Gamezone mais une activités va y prendre beaucoup de place, L'E-sport. Et oui Gamezone sera un lieu qui permettras d'acceuillir des rencontres entre les joueurs Pro, des joueurs débutant, mais aussi des spectateur lors de compétition. </p>
            </div>
          </div>
          <br>
          <div class="card" class="card mb-3">
            <div class="card-body">
              <h2 class="card-title">Envie de jeux rétro ? Venez redécouvrir les jeux qui on bercé votre enfance.</h2>
              <p class="card-text">Dans le parc Gamezone vous allez pouvoir jouer a vos jeux d'enfance pour les plus vieux et découvrir les ancêtres des jeux vidéo d'aujourd'hui. Les bornes de l’époque pour défier vos amis dans les meilleurs jeux retro : Mario, Centipede, QBert, Space invaders, Pac-Man...  </p>
            </div>
          </div>
          <br>
          <div class="card" class="card mb-3">
            <img src="img/playstationvr.jpg" width="75%" alt="Card image cap">
            <div class="card-body">
              <h2 class="card-title">La VR dans Gamezone ?</h2>
              <p class="card-text">Pour les fans de jeux vidéo et nouvelles expériences, le casque Playstation VR est en vente depuis le 13 octobre ! La réalité virtuelle fait irruption dans votre salon et vous invite à vivre des aventures exceptionnelles. Vous pourrez tester cette technologie dans notre parc ! </p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <br>
  <footer>
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <ul>
            <li><a href="parc.php>">A Propos du parc</a></li>
            <li><a href="presse.php">Dans la presse</a></li>
            <li><a href="recrutement.php">On recrute</a></li>
          </ul>
        </div>
        <div class="col">
          <ul>
            <li><a href="mailto:contact@gamezone.com">Nous contacter</a></li>
            <li><a href="conditions_ventes.php">Conditions de vente</a></li>
            <li><a href="conditions_legales.php">Conditions légales</a></li>
          </ul>
        </div>
        <div class="col">
          <ul>
            <li>Du lundi au jeudi : 9h-19h</li>
            <li>Du vendredi au samedi : 9h-20h</li>
            <li>Le dimanche : 9h – 18h</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <img src="img/logo_facebook.png" alt="logo_facebook" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_twitter.png" alt="logo_twitter" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_insta.png" alt="logo_insta" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_youtube.png" alt="logo_youtube" width="50%">
        </div>
        <div class="col-7">
          <form class="input-group">
            <input type="text" class="form-control form-control-sm" placeholder="Votre mail" aria-label="Your email" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="btn btn-sm btn-outline-white" type="button">Inscrivez-vous !</button>
            </div>
          </form>
        </div>


      <div class="row">
        <div class="col">
        <p>©2018 - Tout droits réservés</p>
        </div>
      </div>
  </footer>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
  </html>
