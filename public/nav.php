<?php
if (isset($_SESSION['id'])) { echo'
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="avbar-brand" href="index.php">
  <img src="img/logo_gamezone.png" width="150" height="100" alt="">
</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="parc.php">Le parc</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="attraction.php">Attractions</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="plan.php">Plan</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Préparer ma visite
      </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="road.php">Se rendre au parc</a>
            <a class="dropdown-item" href="price.php">Tarif et billetterie</a>
            <a class="dropdown-item" href="calendar.php">Horaires & Calendrier</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="mygame.php">My game</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="info.php">Informations</a>
        </li>

      </ul>
      <ul class="navbar-nav mr-auto">
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Votre recherche..." aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Go</button>
        </form>
        <li class="nav-item">
          <a class="nav-link" href="profil.php">Profil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="logout.php">Deconnexion</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="index.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        FR
      </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="indexen.html">EN</a>
            <a class="dropdown-item" href="indexesp.html">ESP</a>
          </div>
        </li>
      </ul>

    </div>
  </nav> ' ;
} else { echo'
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="avbar-brand" href="index.php">
  <img src="img/logo_gamezone.png" width="150" height="100" alt="">
</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="parc.php">Le parc</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="attraction.php">Attractions</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="plan.php">Plan</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Préparer ma visite
      </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Se rendre au parc</a>
            <a class="dropdown-item" href="#">Tarif et billetterie</a>
            <a class="dropdown-item" href="#">Horaires & Calendrier</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="mygame.php">My game</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="info.php">Informations</a>
        </li>

      </ul>
      <ul class="navbar-nav mr-auto">
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Votre recherche" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
        </form>
      </ul>
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="inscription.php">Inscription</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="connexion.php">Se connecter</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="index.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        FR
      </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="indexen.html">EN</a>
            <a class="dropdown-item" href="indexesp.html">ESP</a>
          </div>
        </li>
      </ul>

    </div>
  </nav> ' ; }

?>
