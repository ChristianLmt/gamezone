<?php session_start() ; ?>
<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="style/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Satisfy" rel="stylesheet"> 
  <title>Game'zone</title>
</head>

<body>
  <header>
  <?php include 'nav.php' ; ?>
  </header>
  <div class="container">
      <div class="row">
        <h1 class="col-10">Le prix de votre journee</h1>
        <div class="container">

                <article>

                  <label class="col"> Gratuit pour les enfants de 0 à 2 ans </label><br><br>
                  <label class="col"> 12.50 € Pour les enfants de 2 à 8 ans </label><input type="number" id ="enfant" value="0" class="col" onclick="calcul()" min="0"></input><br><br>
                  <label class="col"> 13.50 € Pour les enfants de plus de 8 ans </label><input type="number" id ="Enfant" value="0" class="col" onclick="calcul()" min="0"></input><br><br>
  				        <label class="col"> 15 € Pour les adultes (plus de 18 ans)  </label><input type="number" id ="adult" value="0" class="col" onclick="calcul()" min="0"></input><br><br>
                  <p>Votre journée vous couteras : <span id="prixTotal"></span> €</p>
                  <button class="btn btn-primary" type="submit" onclick="calcul()" ><span class=""></span>Achetez vos billets !</button>
                  <br>
              </article>

      </div>

      </div>
    </div>
  <br>
  <footer>
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <ul>
            <li><a href="parc.php>">A Propos du parc</a></li>
            <li><a href="presse.php">Dans la presse</a></li>
            <li><a href="recrutement.php">On recrute</a></li>
          </ul>
        </div>
        <div class="col">
          <ul>
            <li><a href="mailto:contact@gamezone.com">Nous contacter</a></li>
            <li><a href="conditions_ventes.php">Conditions de vente</a></li>
            <li><a href="conditions_legales.php">Conditions légales</a></li>
          </ul>
        </div>
        <div class="col">
          <ul>
            <li>Du lundi au jeudi : 9h-19h</li>
            <li>Du vendredi au samedi : 9h-20h</li>
            <li>Le dimanche : 9h – 18h</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <img src="img/logo_facebook.png" alt="logo_facebook" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_twitter.png" alt="logo_twitter" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_insta.png" alt="logo_insta" width="50%">
        </div>
        <div class="col">
          <img src="img/logo_youtube.png" alt="logo_youtube" width="50%">
        </div>
        <div class="col-7">
          <form class="input-group">
            <input type="text" class="form-control form-control-sm" placeholder="Votre mail" aria-label="Your email" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="btn btn-sm btn-outline-white" type="button">Inscrivez-vous !</button>
            </div>
          </form>
        </div>


      <div class="row">
        <div class="col">
        <p>©2018 - Tout droits réservés</p>
        </div>
      </div>
  </footer>
    <!-- JS for price.html -->
    <script src="js/price.js" ></script>
    <!-- Optional JavaScript -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
  </html>
