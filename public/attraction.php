<?php session_start() ; ?>
<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="style/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Satisfy" rel="stylesheet"> 
  <title>Game'zone</title>
</head>

<body>
  <header>
  <?php include 'nav.php' ; ?>
  </header>
  </header>
    <div class="container">
      <div class="row">
        <div class="col">
          <h2> Les attractions </h2>
          <div class="card" style="width:100%" "18rem;">
            <img class="card-img-top" src="img/logo_contagion_vr.png" alt="Card image cap" style="width:25%">
            <div class="card-body">
              <h3 class="card-title">Contagion VR</h3>
              <p class="card-text">Gain XP : 1500 XP / partie Interdit au moins d’1m30 <br>
                Vous dirigez une équipe de soldats en charge
                de découvrir ce qui est arrivé aux scientifiques du laboratoire minier Omega Centuri B. <br>
                Une fois à bord, vous apprenez qu’un terrible virus a fait muter tout l’équipage, et que la
                station spatiale est maintenant infestée par d’horribles zombies !
                Vous devez donc évacuer votre équipe en tentant de survivre le plus longtemps possible.</p>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalContagion">
                  Lancer la vidéo
                </button>

                <!-- Modal -->
                <div class="modal fade" id="modalContagion" tabindex="-1" role="dialog" aria-labelledby="modalContagionLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h3 class="modal-title" id="modalContagionLabel">Contagion VR</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/frqDT4bD-OE" allowfullscreen></iframe>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <div class="card" style="width:100%" "18rem;">
              <img class="card-img-top" src="img/logo_battle_kart.png" alt="Card image cap" style="width:25%">
              <div class="card-body">
                <h3 class="card-title">Battle Kart</h3>
                <p class="card-text">Gain XP : 1000 XP / partie Accessible à tous <br>
                  BattleKart, c’est la quintessence du jeu vidéo, de
                  la réalité augmentée et du karting électrique,
                  réunis pour vous procurer des sensations inédites entre amis, en familles ou entre collègues ! <br>
                  C’est avant tout l’un des plus grands écrans de cinéma au monde (deux pistes de 2.000 m2) sur lequel nous projetons différents circuits et modes de jeu,
                  et sur lequel vous évoluez réellement plein gaz à bord d’un kart électrique, tout en interagissant
                  avec le décor, les bonus, et les autres pilotes.</p>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalKart">
                    Lancer la vidéo
                  </button>

                  <!-- Modal -->
                  <div class="modal fade" id="modalKart" tabindex="-1" role="dialog" aria-labelledby="modalKartLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h3 class="modal-title" id="modalKartLabel">Battle Kart</h3>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Ty8ycqgbax0" allowfullscreen></iframe>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


              <div class="card" style="width:100%" "18rem;">
                <img class="card-img-top" src="img/logo_awsome_heroes_team.png" alt="Card image cap" style="width:25%">
                <div class="card-body">
                  <h3 class="card-title">Awsome Heroes Team</h3>
                  <p class="card-text">Gain XP : 1200 XP / partie Interdit au moins d’1m30 <br>
                    Une plongée épique dans l’univers des vikings et des chevaliers.
                    Vous retrouverez l’ambiance du célèbre jeu For Honor du studio français Ubisoft.
                    Une attraction riche en sensations fortes déconseillée aux âmes sensibles.</p>
                    <a href="#" class="btn btn-primary">À venir !</a>
                  </div>
                </div>


                <div class="card" style="width:100%" "18rem;">
                  <img class="card-img-top" src="img/logo_champions_league.png" alt="Card image cap" style="width:25%">
                  <div class="card-body">
                    <h3 class="card-title">Champions League</h3>
                    <p class="card-text">Gain XP : 1300 XP / partie Accessible à tous <br>
                      Dans ce jeu vous bénéficiez de la toute dernière technologie hologramme pour affronter les légendes du football en partenariat avec Konami
                      et la licence de simulation de football PES. Venez affronter sur le terrain Maradona, Pelé, Zidane, Messi et bien d’autres.</p>
                      <a href="#" class="btn btn-primary">À venir !</a>
                    </div>
                  </div>


                  <div class="card" style="width:100%" "18rem;">
                    <img class="card-img-top" src="img/logo_champions_league_survivor.png" alt="Card image cap" style="width:25%">
                    <div class="card-body">
                      <h3 class="card-title">Champions League Survivor</h3>
                      <p class="card-text">Gain XP : 1400 XP / partie Interdit au moins d’1m10 <br>
                        Dans cette version alternative du jeu
                        Champions League, les joueurs de foot
                        sont remplacès par des vampires, des loups
                        garous et des zombies. Vous devrez donc marquer des buts sans vous faire dévorer par d’horribles créatures.</p>
                        <a href="#" class="btn btn-primary">À venir !</a>
                      </div>
                    </div>


                    <div class="card" style="width:100%" "18rem;">
                      <img class="card-img-top" src="img/logo_super_fighter_league.png" alt="Card image cap" style="width:25%">
                      <div class="card-body">
                        <h3 class="card-title">Super Figther League</h3>
                        <p class="card-text">Gain XP : 1000 XP / partie Interdit au moins d’1m10 <br>
                          Vous aimez les jeux de combat alors bienvenue dans ce grand 8 aux couleurs des plus grands combattants de l’histoire du jeu-vidéo :
                          Ryu, Raiden, Akuma, Yoshimitsu, Sub Zero, Scorpion et plus encore vont vous défier dans ce manège à sensation.</p>
                          <a href="#" class="btn btn-primary">À venir !</a>
                        </div>
                      </div>


                      <div class="card" style="width:100%" "18rem;">
                        <img class="card-img-top" src="img/logo_fighter_hard_team.png" alt="Card image cap" style="width:25%">
                        <div class="card-body">
                          <h3 class="card-title">Fighter Hard Team</h3>
                          <p class="card-text">Gain XP : 1000 XP / partie Interdit au moins d’1m30 <br>
                            Dans ce jeu, il n’y a plus de règle. Vous êtes équipé de la dernière armure sensitive et vous ressentirez tous les coups que vos adversaires vont vous porter.
                            Attention donc à ne pas prendre de mauvais coups. Une attraction déconseillée aux femmes enceintes.</p>
                            <a href="#" class="btn btn-primary">À venir !</a>
                          </div>
                        </div>


                        <div class="card" style="width:100%" "18rem;">
                          <img class="card-img-top" src="img/logo_game_center.png" alt="Card image cap" style="width:25%">
                          <div class="card-body">
                            <h3 class="card-title">Game Center</h3>
                            <p class="card-text">Gain XP : 500 XP / partie
                              Accessible à tous <br>
                              Un espace de 3000 m2 dédié au Retro Gaming. Vous retrouverez toute l’ambiance des salles d’arcade des années 80
                              avec les bornes de l’époque pour défier vos amis dans les meilleurs jeux retro : Mario, Centipede, Q Bert, Space invaders, Pac-Man...</p>
                              <a href="#" class="btn btn-primary">À venir !</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>



                    <footer>
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col">
                            <ul>
                              <li><a href="parc.php>">A Propos du parc</a></li>
                              <li><a href="presse.php">Dans la presse</a></li>
                              <li><a href="recrutement.php">On recrute</a></li>
                            </ul>
                          </div>
                          <div class="col">
                            <ul>
                              <li><a href="mailto:contact@gamezone.com">Nous contacter</a></li>
                              <li><a href="conditions_ventes.php">Conditions de vente</a></li>
                              <li><a href="conditions_legales.php">Conditions légales</a></li>
                            </ul>
                          </div>
                          <div class="col">
                            <ul>
                              <li>Du lundi au jeudi : 9h-19h</li>
                              <li>Du vendredi au samedi : 9h-20h</li>
                              <li>Le dimanche : 9h – 18h</li>
                            </ul>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <img src="img/logo_facebook.png" alt="logo_facebook" width="50%">
                          </div>
                          <div class="col">
                            <img src="img/logo_twitter.png" alt="logo_twitter" width="50%">
                          </div>
                          <div class="col">
                            <img src="img/logo_insta.png" alt="logo_insta" width="50%">
                          </div>
                          <div class="col">
                            <img src="img/logo_youtube.png" alt="logo_youtube" width="50%">
                          </div>
                          <div class="col-7">
                            <form class="input-group">
                              <input type="text" class="form-control form-control-sm" placeholder="Votre mail" aria-label="Your email" aria-describedby="basic-addon2">
                              <div class="input-group-append">
                                <button class="btn btn-sm btn-outline-white" type="button">Inscrivez-vous !</button>
                              </div>
                            </form>
                          </div>


                        <div class="row">
                          <div class="col">
                          <p>©2018 - Tout droits réservés</p>
                          </div>
                        </div>
                    </footer>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
