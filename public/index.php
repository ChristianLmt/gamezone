<?php session_start() ; ?>
<!doctype html>
<html lang="fr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="style/style.css">
  <link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Satisfy" rel="stylesheet">
  <title>Game'zone</title>
</head>

<body>
  <header>
  <?php include 'nav.php' ; ?>
  </header>
  <div class="page">
    <h1>Game'Zone</h1>
    <img src="img/photo_parc2.jpg" width="80%" alt="photo_du_parc">
    <div class="container">
      <div class="row">
        <div class="col">
          <h2> Une nouvelle ère </h2>
          <p>Bienvenue sur le site du premier parc d'attraction principalement dédié aux jeux-videos.</p>
          <p>Passer une journée à Game'Zone c'est passer un super moment détente et découvrir une gamme d'attraction originale et innovente adaptée à toute la famille.</p>
        </div>
        <div class="meteo">
        <div class="col">
          <h2>Pourquoi ne pas venir aujourd'hui ?</h2>
          <?php
            $jour=date("d");
            $mois=date("m");
            $annee=date("Y");
            echo "Nous sommes le $jour/$mois/$annee";
            ?></br>
          <script charset='UTF-8' src='http://www.meteofrance.com/mf3-rpc-portlet/rest/vignettepartenaire/301890/type/VILLE_FRANCE/size/PAYSAGE_VIGNETTE' type='text/javascript'>
</script></br>
          <h3><a href="price.php">Acheter votre billet !</a></h3>
        </div>
      </div>
      </div>
    </div>
  <div class="container">
    <h2>Nos attractions phares</h2>
    <div class="row">
      <div class="col">
        <h2>Contagion VR</h2>
        <img src="img/logo_contagion_vr.png" alt="logo_contagion" width="60%">
        <h3>Interdit au moins d’1m30</h3>
        <p>Vous dirigez une équipe de soldats en charge de découvrir ce qui est arrivé aux scientifiques du laboratoire minier Omega Centuri B.</p>
        <p>Une fois à bord, vous apprenez qu’un terrible virus a fait muter tout l’équipage, et que la station spatiale est maintenant infestée par d’horribles zombies !</p>
        <p>Vous devez donc évacuer votre équipe en tentant de survivre le plus longtemps possible.</p>
      </div>
      <div class="col">
        <h2>Battle Kart</h2>
        <img src="img/logo_battle_kart.png" alt="logo_battlekart" width="59%">
        </br>
        </br>
        </br>
        </br>
        <h3>Accessible à toute la famille !</h3>
        <p>BattleKart, c’est la quintessence du jeu vidéo, de la réalité augmentée et du karting électrique, ré- unis pour vous procurer des sensations inédites entre amis, en familles ou entre collègues !</p>
        <p>C’est avant tout l’un des plus grands écrans de cinéma au monde (deux pistes de 2.000 m2) sur lequel nous projetons différents circuits et modes de jeu, et sur lequel vous évoluez réellement plein gaz à bord d’un kart électrique, tout en interagissant
          avec le décor, les bonus, et les autres pilotes.</p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <h2>Awsome heroes team</h2>
        <img src="img/logo_awesome.png" alt="logo_awesome" width="50%">
        <h3>Interdit au moins d’1m30</h3>
        <p>Une plongée épique dans l’univers des vikings et des chevaliers. Vous retrouverez l’ambiance du célèbre jeu For Honor du studio français Ubisoft.</p>
        <p>Une attraction riche en sensations fortes déconseillée aux âmes sensibles.</p>
      </div>
      <div class="col">
        <h2>Champions League<h2>
              <img src="img/logo_champions.png" alt="logo_champion" width="50%" >
              <h3>Accessible à toute la famille !</h3>
              <p>Dans ce jeu vous bénéficiez de la toute dernière technologie hologramme pour affronter les légendes du football en partenariat avec Konami et la licence de simulation de football PES.</p>
              <p>Venez affronter sur le terrain Maradona, Pelé, Zidane, Messi et bien d’autres.</p>
        </div>
      </div>
    </div>
        <img src="img/photo_parc1.jpg" width="80%" alt="photo_parc1">
  </div>
<br>
<footer>
  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <ul>
          <li><a href="parc.php>">A Propos du parc</a></li>
          <li><a href="presse.php">Dans la presse</a></li>
          <li><a href="recrutement.php">On recrute</a></li>
        </ul>
      </div>
      <div class="col">
        <ul>
          <li><a href="mailto:contact@gamezone.com">Nous contacter</a></li>
          <li><a href="conditions_ventes.php">Conditions de vente</a></li>
          <li><a href="conditions_legales.php">Conditions légales</a></li>
        </ul>
      </div>
      <div class="col">
        <ul>
          <li>Du lundi au jeudi : 9h-19h</li>
          <li>Du vendredi au samedi : 9h-20h</li>
          <li>Le dimanche : 9h – 18h</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <img src="img/logo_facebook.png" alt="logo_facebook" width="50%">
      </div>
      <div class="col">
        <img src="img/logo_twitter.png" alt="logo_twitter" width="50%">
      </div>
      <div class="col">
        <img src="img/logo_insta.png" alt="logo_insta" width="50%">
      </div>
      <div class="col">
        <img src="img/logo_youtube.png" alt="logo_youtube" width="50%">
      </div>
      <div class="col-7">
        <form class="input-group">
          <input type="text" class="form-control form-control-sm" placeholder="Votre mail" aria-label="Your email" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-sm btn-outline-white" type="button">Inscrivez-vous !</button>
          </div>
        </form>
      </div>


    <div class="row">
      <div class="col">
      <p>©2018 - Tout droits réservés</p>
      </div>
    </div>
</footer>

      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
